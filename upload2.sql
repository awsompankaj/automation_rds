

CREATE DATABASE  `newbookfinal` ;

USE `newbookfinal`;
DROP TABLE IF EXISTS `authors2`;

CREATE TABLE `authors2` (
  `id` int,
  `name` varchar(20),
  `email` varchar(20)
);


LOCK TABLES `authors2` WRITE;

INSERT INTO `authors2` VALUES (1,'Vivek','xuz@abc.com'),(3,'Tom','tom@yahoo.com'),(3,'Vivek','xuz@abc.com'),(4,'Tom','tom@yahoo.com'),(5,'Vivek','xuz@abc.com'),(6,'Tom','tom@yahoo.com'),(7,'Vivek','xuz@abc.com'),(8,'Tom','tom@yahoo.com'),(8,'Vivek','xuz@abc.com'),(9,'Tom','tom@yahoo.com');

UNLOCK TABLES;
