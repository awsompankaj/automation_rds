-- Switch to the sample database
USE sample_db; CREATE TABLE employees (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  age INT,
  salary DECIMAL(10,2)
);INSERT INTO employees (name, age, salary) VALUES
  ('John Doe', 30, 5000.00),
  ('Jane Smith', 28, 4500.00),
  ('Michael Johnson', 35, 6000.00);
