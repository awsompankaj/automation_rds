# Configure AWS provider
provider "aws" {
  region                  = "ap-south-1"  # Update with your desired AWS region
  # access_key              = "${AWS_ACCESS_KEY_ID}"
  # secret_key              = "${AWS_SECRET_ACCESS_KEY}"
  profile                 = "mayank"
}



# Create an AWS Secrets Manager secret for RDS credentials
resource "aws_secretsmanager_secret" "rds_credentials" {
  name = "rds-credential1"
}

# Generate a random password for RDS instance
# resource "random_password" "rds_password" {
#   length           = 16
#   special          = true
#   override_special = "!@#$%^&*()_+{}:\"<>?|=[]\\;',./`~"
# }

# Store the RDS username and password in the Secrets Manager secret
resource "aws_secretsmanager_secret_version" "rds_credentials_version" {
  secret_id     = aws_secretsmanager_secret.rds_credentials.id
  secret_string = jsonencode({
    username = "admin1"
    # password = random_password.rds_password.result
    password = "asdfhnEr3aajHw"
  })
}

# Create a security group for RDS instance
resource "aws_security_group" "rds_sg" {
  name        = "rds-security-group"
  description = "Security group for RDS instance"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create RDS instance
resource "aws_db_instance" "rds_instance" {
  identifier            = "my-rds-instance"
  engine                = "mysql"
  instance_class        = "db.t2.micro"
  allocated_storage     = 20
  storage_type          = "gp2"
  # username              = jsondecode(aws_secretsmanager_secret_version.rds_credentials_version.secret_string)["username"]
  # password              = jsondecode(aws_secretsmanager_secret_version.rds_credentials_version.secret_string)["password"]
  username = "admin1" 
  password = "asdfhnEr3aajHw" 
  publicly_accessible  = true
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
  skip_final_snapshot          = true
}

# Output the RDS instance details
output "rds_instance_endpoint" {
  value = aws_db_instance.rds_instance.endpoint
}
